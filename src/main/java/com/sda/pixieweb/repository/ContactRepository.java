package com.sda.pixieweb.repository;

import com.sda.pixieweb.model.Contact;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ContactRepository extends JpaRepository <Contact,Long> {
}
