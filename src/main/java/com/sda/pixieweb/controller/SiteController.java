package com.sda.pixieweb.controller;

import com.sda.pixieweb.model.Contact;
import com.sda.pixieweb.repository.ContactRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class SiteController {

    @Autowired
    private ContactRepository contactRepository;


    @PostMapping("/addToTheDB")
    public String addToTheDB(@RequestParam("nume") String nume, @RequestParam("prenume") String prenume, @RequestParam("email") String email, @RequestParam("mesaj") String mesaj) {
        Contact contact=new Contact();
        contact.setNume(nume);
        contact.setPrenume(prenume);
        contact.setEmail(email);
        contact.setMesaj(mesaj);
         contactRepository.save(contact);
        return "redirect:http://localhost:8080/trimitere.html";
    }


}
