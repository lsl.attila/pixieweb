package com.sda.pixieweb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PixiewebApplication {

	public static void main(String[] args) {
		SpringApplication.run(PixiewebApplication.class, args);
	}

}
