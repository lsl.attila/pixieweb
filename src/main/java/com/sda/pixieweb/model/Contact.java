package com.sda.pixieweb.model;

import javax.persistence.*;

@Entity
public class Contact {



    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column
    private String nume;
    @Column
    private String prenume;
    @Column
    private String email;
    @Column
    private String mesaj;

    public Contact(){

    }

    public Contact(Long id, String nume, String prenume, String email, String mesaj) {
        this.id = id;
        this.nume = nume;
        this.prenume = prenume;
        this.email = email;
        this.mesaj = mesaj;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNume() {
        return nume;
    }

    public void setNume(String nume) {
        this.nume = nume;
    }

    public String getPrenume() {
        return prenume;
    }

    public void setPrenume(String prenume) {
        this.prenume = prenume;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMesaj() {
        return mesaj;
    }

    public void setMesaj(String mesaj) {
        this.mesaj = mesaj;
    }
}
